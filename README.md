# Gulp Starter for CSS Tricks Tutorial  

This repo is a gulp starter for this CSS Tricks tutorial. 

Installation 
1. Use `npm install` to install the package.
2. Use `gulp serve` to compile and run pages on localhost.
3. Use `gulp build` to build the project.
4. Use `gulp allclean` to clean temporary and built project files.

Remember to do run the `npm install` command after cloning this repo :) 

