var gulp = require('gulp');
var pug = require('gulp-pug');
var sass = require('gulp-sass');
var browserSync = require('browser-sync').create();
var reload = browserSync.reload;
var useref = require('gulp-useref');
var imagemin = require('gulp-imagemin');
var cache = require('gulp-cache');
var del = require('del');
var runSequence = require('run-sequence');
var gulpLoadPlugins = require('gulp-load-plugins');
var $ = gulpLoadPlugins();
var concat = require('gulp-concat');
var rename = require('gulp-rename');
var uglify = require('gulp-uglify');

var minify = require('minify');
var gulpIf = require('gulp-if');
var cssnano = require('gulp-cssnano');
const browserify = require('browserify');
var media = require('gulp-pipe-media');



// variables 


// Start browserSync server
// gulp.task('browserSync', function() {
//   browserSync({
//     server: {
//       baseDir: 'app'
//     }
//   })
// })

// gulp.task('imports', function() {
//   gulp.src(['app/clientlibs/js/**/*.js'])
//       .pipe(gulpImports())
//       .pipe(gulp.dest('.tmp/clientlibs/js'));
// });

gulp.task('browser-sync', function() {
  browserSync.init({
      server: {
          // baseDir: "app"
          baseDir:['.tmp', 'app']
      },
      // proxy: "localhost:8080" // makes a proxy for localhost:8080
  });
});


gulp.task('pug', () =>  {
  return gulp.src(['app/html/[^_]*.pug'])
    .pipe(pug({
      pretty: true
    })
    )
    .pipe(gulp.dest('.tmp'))
});

gulp.task('pug-mashup', () =>  {
    return gulp.src(['app/html/mashups/[^_]*.pug'])
        .pipe(pug({
                pretty: true
            })
        )
        .pipe(gulp.dest('.tmp/mashups'))
});


gulp.task('pug-html', () =>  {
  return gulp.src([ 
    'app/html/[^_]*.pug'
    // '!app/html/default-template.pug'
  ])
    .pipe(pug({
      pretty: true
    })
    )
    .pipe(gulp.dest('.tmp'))
    p
});


gulp.task('sass', function() {
  return gulp.src('app/clientlibs/scss/*.scss') // Ge9ts all files ending with .scss in app/scss and children dirs
    .pipe(sass().on('Sass error', sass.logError)) // Passes built it th2rough a gul0p-sass, 
    .pipe(gulp.dest('.tmp/clientlibs/css')) // Outputs it in by the css folder
    .pipe(browserSync.reload({ // Reloading with Browser Sync
      stream: true
    }));
})

gulp.task('sass-css', function() {
  return gulp.src([
    'app/clientlibs/scss/*.scss'
    // '!app/clientlibs/scss/components.scss'
  ]) 
    .pipe(sass().on('error', sass.logError)) 
    .pipe(gulp.dest('.tmp/clientlibs/css')) 
    .pipe(browserSync.reload({ 
      stream: true
    }));
})


// Watchers
gulp.task('watch', ['browser-sync'], function() {
  gulp.watch('app/clientlibs/scss/**/*.scss', ['sass']);
  gulp.watch('app/html/*.*/*.pug', browserSync.reload);
  gulp.watch('app/clientlibs/js/**/*.js', browserSync.reload);
  gulp.watch("app/html/*.pug").on('change', browserSync.reload);
})

// Optimizing CSS and JavaScript 
gulp.task('useref', function() {
  return gulp.src(['app/html/*.html', '.tmp/*.html'])
    .pipe(useref())
    .pipe($.if(/\.css$/, $.cssnano({safe: true, autoprefixer: false})))
    .pipe($.if(/\.html$/, $.htmlmin({
      collapseWhitespace: false,
      minifyCSS: false,
      minifyJS: {compress: {drop_console: true}},
      processConditionalComments: true,
      removeComments: false,
      removeEmptyAttributes: true,
      removeScriptTypeAttributes: true,
      removeStyleLinkTypeAttributes: true
    })))
    .pipe(gulp.dest('.tmp'));
});

// gulp.task('useref', function() {

//   return gulp.src('.tmp/*.html')
//     .pipe(useref())
//     .pipe(gulpIf('.tmp/clientlibs/js/*.js', uglify()))
//     .pipe(gulpIf('.tmp/clientlibs/css/*.*/*.css', cssnano()))
//     .pipe(gulp.dest('dist'));
// });

// Optimizing Images 
gulp.task('images', function() {
  return gulp.src('app/clientlibs/images/**/*.+(png|jpg|jpeg|gif|svg|gif)')
    // Caching images that ran through imagemin
    .pipe(cache(imagemin({
      interlaced: true,
    })))
    .pipe(gulp.dest('.tmp/clientlibs/images'))
});


gulp.task('videos', function() {
  return gulp.src('app/clientlibs/videos/**/*.*')
    // Caching images that ran through imagemin
  
    .pipe(gulp.dest('.tmp/clientlibs/videos'))
});

gulp.task('audio', function() {
  return gulp.src('app/clientlibs/audio/**/*.*')
    // Caching images that ran through imagemin
  
    .pipe(gulp.dest('.tmp/clientlibs/audio'))
});

gulp.task('docs', function() {
  return gulp.src('app/clientlibs/documents/**/*.*')
    // Caching images that ran through imagemin
  
    .pipe(gulp.dest('.tmp/clientlibs/documents'))
});

// 'app/clientlibs/images/**/*.+(png|jpg|jpeg|gif|svg)',
// '!app/clientlibs/images/**/_*',
//     '!app/clientlibs/images/**/_*/**/*'

gulp.task('images-pack', function() {
  return gulp.src([
    'app/clientlibs/images/**/*',
    '!app/clientlibs/images/**/_*',
    '!app/clientlibs/images/**/_*/**/*'
    
    
  ])
    // Caching images that ran through imagemin
    .pipe(cache(imagemin({
      interlaced: true,
    })))
    .pipe(gulp.dest('.tmp/clientlibs/images'))
});

// gulp.task('js', function () {  
//   // return gulp.src(['app/clientlibs/js/*.*', 'assets/js/main.js', 'assets/js/module*.js'])
//   return gulp.src(['app/clientlibs/js/**/*.js'])
//     // .pipe(concat('bundle.js'))
//     .pipe(gulp.dest('.tmp/clientlibs/js'))
// });

// JS
// gulp.task('js', function () {
//   gulp.src([
//     'app/clientlibs/js/**/*.js'
//   ])
//     .pipe(concat('default-template-script.js'))
//     // .pipe(uglify())
//     .pipe(gulp.dest('.tmp/clientlibs/js'))
//     .pipe(browserSync.stream());
// });

gulp.task('json', function() {
    return gulp.src([
        'app/clientlibs/*.json'
    ]).pipe(gulp.dest('.tmp/clientlibs/'));
});


gulp.task('js', function() {
    return gulp.src([
      'app/clientlibs/js/**/*.js'
    ])
        .pipe(concat('scripts.js'))
        .pipe(gulp.dest('.tmp/clientlibs/js'));
});

gulp.task('js-plugins', function() {
  return gulp.src([
    'node_modules/jquery/dist/jquery.js',
    'node_modules/bootstrap/dist/js/bootstrap.min.js',
    'node_modules/slick-carousel/slick/slick.js',
    'node_modules/aos/dist/aos.js',
    'node_modules/dateformat/lib/dateformat.js'

  ])
    .pipe(gulp.dest('.tmp/clientlibs/js/plugins'));
});



// JS
// gulp.task('js-cmd', function () {
//   gulp.src([
//     // 'app/clientlibs/js/**/*.js',
//     // '!app/clientlibs/js/default-template-script.js'
//     'app/clientlibs/js/project-script.js'
//   ])
//     .pipe(concat('project-script.js'))
//     // .pipe(uglify())
//     .pipe(gulp.dest('.tmp/clientlibs/js'))
//     .pipe(browserSync.stream());
// });

// gulp.task('js2', () => {
//   const b = browserify({
//     entries: 'app/clientlibs/js/project-script.js',
//     transform: ['babelify', 'pugify'],
//     debug: true,
//   },{
//     paths:['app/clientlibs/js/components']
//   });
//   return b.bundle()
//     .pipe(source('project-script.js'))
//     .pipe($.plumber())
//     .pipe(buffer())
//     .pipe($.sourcemaps.init({loadMaps: true}))
//     .pipe($.if(dev, $.sourcemaps.write('.')))
//     .pipe(gulp.dest('.tmp/assets/js'))
//     .pipe(reload({stream: true}));
// });


// Copying fonts 
gulp.task('fonts', function() {
  return gulp.src('app/clientlibs/fonts/**/*')
    .pipe(gulp.dest('.tmp/clientlibs/fonts'))
})

// Cleaning 
// gulp.task('clean', function() {
//   return del.sync(['dist','.tmp']).then(function(cb) {
//     return cache.clearAll(cb);
//   });
// })

// gulp.task('allclean', del.bind(null, ['.tmp', '.tmp/**/*', 'dist']));

gulp.task('allclean', function() {
  return del.sync(['.tmp/**/*', '!.tmp/images', '!.tmp/images/**/*', '.tmp', 'dist/**/*','dist']);
});

gulp.task('distclean', function() {
  return del.sync(['dist/**/*', '!dist/images', '!dist/images/**/*']);
});

// Build Sequences
// ---------------

gulp.task('default', function(callback) {
  runSequence(['sass', 'browserSync'], 'watch',
    callback
  )
})

gulp.task('serve', () => {
  runSequence(['allclean'], ['pug', 'sass', 'fonts', 'js', 'js-plugins', 'images', 'docs', 'videos', 'audio', 'json'], () => {
    browserSync.init({
     // online: true,
      notify: false,
      port: 5500,
      //index: "home.html",
      server: {
        baseDir: ['.tmp', 'app'],
        routes: {
          '/node_modules': 'node_modules'
        }
      }
    });

    gulp.watch([
      'app/**/*',
      'app/html/components/*.pug',
      'app/html/mashups/*.pug',
      'app/clientlibs/scss/*.scss',
      'app/clientlibs/images/**/*.*',
      'app/clientlibs/js/**/*',
      'app/clientlibs/*.json',
      '.tmp/clientlibs/fonts/**/*',
      '.tmp/clientlibs/js/**/*',
      '.tmp/clientlibs/css/**/*'
    ]).on('change', reload);

    gulp.watch(['app/**/*.pug','!app/html/vue-components/*.pug'], ['pug']);
    gulp.watch('app/html/vue-components/*.pug', ['js']);
    gulp.watch('app/clientlibs/scss/**/*.scss', ['sass']);
    gulp.watch('app/clientlibs/js/**/*.js', ['js']);
    gulp.watch('app/clientlibs/*.json', ['json']);
    gulp.watch('app/clientlibs/fonts/**/*', ['fonts']);
    gulp.watch('app/clientlibs/images/**/*', ['images']);
    // gulp.watch('app/assets/data/*', ['dummy-data', 'js']);
  });
});


gulp.task('build', ['distclean', 'pug', 'sass', 'images', 'fonts', 'js', 'js-plugins', 'docs','videos', 'audio', 'json','pug-mashup'], () => {
  dev = false;
  return gulp.src('.tmp/**/*').pipe(gulp.dest('dist'));
});
