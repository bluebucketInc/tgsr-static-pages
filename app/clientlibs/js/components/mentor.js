(function ($) {

    let _mentorCarousel, _mentorPopover;


    function _slickInitHandler(e) {
        _mentorCarousel.fadeIn();
    }

    function _slickBeforeChangeHandler(e, slick, currentSlide, nextSlide) {
        const slideCount = slick.slideCount - 1;
        const nextSlideIsFirst = currentSlide === slideCount;
        const nextSlideIsLast = nextSlide === slideCount;
        const $activeSlide = $('.slick-slide.slick-current');
        if (nextSlideIsFirst && nextSlide == 0) {
            $activeSlide.next('.slick-slide').addClass('slick-clone-current').attr("data-aria-hidden", "false");
            $activeSlide.next('.slick-slide').next(".slick-slide").attr("data-aria-hidden", "false");
        } else if (nextSlideIsLast && currentSlide == 0) {
            $activeSlide.prev('.slick-slide').addClass('slick-clone-current').attr("data-aria-hidden", "false");
            $activeSlide.prev('.slick-slide').prev(".slick-slide").attr("data-aria-hidden", "false");
        }

        _mentorCarousel.find('[aria-hidden="false"]').attr("data-aria-hidden", "false");
    }

    function _slickAfterChangeHandler(e, slick, currentSlide, nextSlide) {
        $('.slick-clone-current').removeClass('slick-clone-current');
        _mentorCarousel.find('[data-aria-hidden="false"]').attr("data-aria-hidden", "");
    }

    function _hashChangeHandler(location) {
        _mentorPopover.hide();
        if (location) {
            $("html, body").animate({scrollTop: 0}, 500);
            var $mentorSlides = _mentorPopover.find(".mentor-slide").hide();

            $mentorSlides.filter(function (i, mentorSlideElement) {
                return $(mentorSlideElement).attr("data-id") == location;
            }).show(100, function(){
                _mentorPopover.fadeIn();
            });
        }
    }

    function _closeOverlayHandler(e) {
        $(".popover").fadeOut();
    }

    function _addEvents() {
        var hash = window.location.hash;
        _mentorCarousel.on("init", _slickInitHandler);
        _mentorCarousel.on('beforeChange', _slickBeforeChangeHandler);
        _mentorCarousel.on('afterChange', _slickAfterChangeHandler);

        $(document).on("click", "[overlay-dialog]", function (e) {
            e.preventDefault();
            var href = $(e.currentTarget).attr("href");
            if (href) {
                _hashChangeHandler(href)
            }
        });


        $(document).on("click", ".close-overlay", _closeOverlayHandler)
        if (window.location.hash){
            _hashChangeHandler(hash);    
        }
    }

    function _initializeCarousal() {
        _mentorCarousel.randomize(".mentor-item");
        _mentorCarousel.slick({
            centerMode: true,
            centerPadding: '60px',
            slidesToShow: 3,
            dots: true,
            speed: 1000,
            draggable: false,
            responsive: [
                {
                    breakpoint: 768,
                    settings: {
                        arrows: true,
                        centerMode: true,
                        centerPadding: '40px',
                        slidesToShow: 1
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        arrows: true,
                        centerMode: true,
                        centerPadding: '40px',
                        slidesToShow: 1
                    }
                }
            ]
        });
    }

    function _variablesInitialize() {
        _mentorCarousel = $('.mentors-carousel');
        _mentorPopover = $(".mentor-detail.popover");
    }

    function _indentText() {
        $('.mentor-slide:nth-child(4) p:nth-child(n+5)').css({'margin-left': '25px', 'display': 'list-item'})
    }

    function _init() {
        _variablesInitialize();
        _addEvents();
        _initializeCarousal();
        //_indentText();
    }


    $(_init());
})(jQuery);
