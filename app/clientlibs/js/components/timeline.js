(function ($) {

    let _$carousel, _mentorPopover, _selectedMode;

    function _slickInitHandler(e) {
        _$carousel.fadeIn();
    }

    function _slickBeforeChangeHandler(e, slick, currentSlide, nextSlide) {
        const slideCount = slick.slideCount - 1;
        const nextSlideIsFirst = currentSlide === slideCount;
        const nextSlideIsLast = nextSlide === slideCount;
        const $activeSlide = $('.slick-slide.slick-current');
        if (nextSlideIsFirst && nextSlide == 0) {
            $activeSlide.next('.slick-slide').addClass('slick-clone-current').attr("data-aria-hidden", "false");
            $activeSlide.next('.slick-slide').next(".slick-slide").attr("data-aria-hidden", "false");
        } else if (nextSlideIsLast && currentSlide == 0) {
            $activeSlide.prev('.slick-slide').addClass('slick-clone-current').attr("data-aria-hidden", "false");
            $activeSlide.prev('.slick-slide').prev(".slick-slide").attr("data-aria-hidden", "false");
        }

        _$carousel.find('[aria-hidden="false"]').attr("data-aria-hidden", "false");
    }

    function _slickAfterChangeHandler(e, slick, currentSlide, nextSlide) {
        $('.slick-clone-current').removeClass('slick-clone-current');
        _$carousel.find('[data-aria-hidden="false"]').attr("data-aria-hidden", "");
    }

    function _toggleHandler(e) {
       _toggleState($(e.target).prop("checked") ? "tunes" : "times");
    }

    function _toggleState(selectedState) {
        var mode, $elements = $(".timeline-times-background, .timeline-tunes-background, .times-title, .tunes-title, .times-carousel, .tunes-carousel");

        if (selectedState.indexOf("#") > -1) {
            selectedState = selectedState.replace(/\#/g, "");
        }

        mode = selectedState.split('_')[0];

        if (mode && mode != _selectedMode) {

            $elements.removeClass("active").filter(function (i, element) {
                return $(element).attr("class").indexOf(mode) > -1 && $(element).attr("class").indexOf("carousel") == -1;
            }).addClass("active");

            _buildCarousal(mode);
        }

    }

    function _addEvents() {
        $(document).on("change", ".timeline-toggler input", _toggleHandler);
    }

    function _getSlickSettings(mode) {
        var settings = {
            "tunes": {
                centerMode: true,
                variableWidth: true,
                // centerPadding: '60px',
                speed: 1400,
                dots: true,
                slidesToShow: 1,
                adaptiveHeight: true,
                responsive: [
                    {
                        breakpoint: 768,
                        settings: {
                            speed: 1000,
                            arrows: true,
                            centerMode: true,
                            centerPadding: '40px',
                            slidesToShow: 1
                        }
                    },
                    {
                        breakpoint: 480,
                        settings: {
                            speed: 1000,
                            arrows: true,
                            centerMode: true,
                            centerPadding: '40px',
                            slidesToShow: 1
                        }
                    }
                ]
            },
            "times": {
                centerMode: true,
                variableWidth: true,
                // centerPadding: '60px',
                speed: 1400,
                dots: true,
                adaptiveHeight: true,
                slidesToShow: 1,
                responsive: [
                    {
                        breakpoint: 768,
                        settings: {
                            speed: 1000,
                            arrows: true,
                            centerMode: true,
                            centerPadding: '40px',
                            slidesToShow: 1
                        }
                    },
                    {
                        breakpoint: 480,
                        settings: {
                            speed: 1000,
                            arrows: true,
                            centerMode: true,
                            centerPadding: '40px',
                            slidesToShow: 1
                        }
                    }
                ]
            }
        };

        return settings[mode]? JSON.parse(JSON.stringify(settings[mode])): {};
    }

    function _initializeCarousal() {
        var mode = "times";
        if(location.hash){
            mode =  location.hash.replace(/\#/g, "").split("_")[0] == "tunes"? "tunes" : "times";
        }

        if(mode == "tunes" && !$(".timeline-toggler input").prop("checked")){
            $(".timeline-toggler input").prop("checked", true);
        }

        _toggleState(mode);
    }

    function _buildCarousal(mode){
        if(_$carousel && _$carousel.get(0)){
            _$carousel.hide();
            _$carousel.slick('unslick');
            _$carousel.removeClass("active");
            _$carousel.off("init", _slickInitHandler);
        }

        _selectedMode = mode;

        _$carousel = $("." + mode + "-carousel");
        _$carousel.on("init", _slickInitHandler);
        _$carousel.on('beforeChange', _slickBeforeChangeHandler);
        _$carousel.on('afterChange', _slickAfterChangeHandler);
        _$carousel.slick(_getSlickSettings(mode));
    }

    function _variablesInitialize() {

        _mentorPopover = $(".mentor-detail.popover");
    }

    function _init() {
        _variablesInitialize();
        _addEvents();
        _initializeCarousal();
    }

    $(_init());
})(jQuery);
