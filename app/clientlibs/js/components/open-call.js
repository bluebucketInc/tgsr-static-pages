(function ($) {
    var _sections, _nav, _nav_height, _navbar, _sticky, _formValidation, _isConsentRequired = false;
 
    function _navigationHandler() {
        var $el = $(this)
            , id = $el.attr('href');
 
        $('html, body').animate({
            scrollTop: $(id).offset().top - _nav_height
        }, 500);
 
        return false;
    }
 
    function _windowScrollHandler() {
        var cur_pos = $(this).scrollTop();
 
        _sections.each(function () {
            var top = $(this).offset().top - _nav_height,
                bottom = top + $(this).outerHeight();
 
            if (cur_pos >= top && cur_pos <= bottom) {
                _nav.find('a').removeClass('active');
                _sections.removeClass('active');
 
                $(this).addClass('active');
                _nav.find('a[href="#' + $(this).attr('id') + '"]').addClass('active');
            }
        });
 
        if (window.pageYOffset >= _sticky) {
            _navbar? _navbar.classList.add("_sticky") : null;
        } else {
            _navbar? _navbar.classList.remove("_sticky") : null;
        }
    }
   
    function _signUpResetHandler(e) {
        $(e.currentTarget).find("button[type='submit']").prop("disabled", true).closest(".expanding-btn").addClass("disabled");
    }
 
    function _signUpHandler(e) {
        e.preventDefault();
        var formData = {};
        $(e.currentTarget).serializeArray().map(function (item) {
            formData[item.name] = item.value;
        });
 
        try{
            $.ajax("/api/create", {
                type: "post",
                dataType: "json",
                data: formData,
                success: function (e) {
                    $("#sucess-msg").fadeIn();
                },
                error: function (e) {
                    $("#error-msg").fadeIn();
                },
                complete: function (e) {
                    $("#sign-up").hide();
                }
            })
        } catch (e) {
            $("#signup").hide();
            $("#error-msg").fadeIn();
        }
 
 
    }
 
    function _nameValidation($this) {
        _formValidation.data.name = /^[a-zA-Z]+([ ]?[a-zA-Z])*$/g.test($this.val());
        return _formValidation.data.name ? "valid" : "invalid";
    }
 
    function _emailValidation($this) {
        _formValidation.data.email = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/g.test($this.val());
        return _formValidation.data.email ? "valid" : "invalid";
    }
 
    function _contactNumberValidation($this) {
        _formValidation.data.contantNumber = /^(\d{8})$/g.test($this.val());
        return _formValidation.data.contantNumber ? "valid" : "invalid";
    }
 
    function _artistNameValidation($this) {
        _formValidation.data.artistName = true;
        return _formValidation.data.artistName ? "valid" : "invalid";
    }
 
    function _ageValidation($this) {
        _formValidation.data.age = /^(\d{2,3})$/g.test($this.val()) && parseInt($this.val()) >= 15;
        _isConsentRequired = false;
        _formValidation.data.consent = false;
 
        var $consent = $("#styled-checkbox-3").closest(".form-group.checkboxes").hide();
 
        if(_formValidation.data.age && parseInt($this.val()) < 18){
            _isConsentRequired = true;
            $consent.fadeIn();
        }
        return _formValidation.data.age ? "valid" : "invalid";
    }
 
    function _webVideoLinkValidation($this) {
        _formValidation.data.webVideoLink = /[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]*)?/g.test($this.val());
        return _formValidation.data.webVideoLink ? "valid" : "invalid";
    }
 
    function _termsValidation($this) {
        _formValidation.data.termsValidation = $this.prop("checked");
 
        // if(_formValidation.data.termsValidation){
        //     window.open("clientlibs/documents/tgsr_tnc.pdf", "_blank");
        // }
 
        return _formValidation.data.termsValidation ? "valid" : "invalid";
    }
 
    function _agreeValidation($this) {
        _formValidation.data.agreeValidation = $this.prop("checked");
        return _formValidation.data.agreeValidation ? "valid" : "invalid";
    }
 
    function _consentValidation($this) {
        _formValidation.data.consent = $this.prop("checked");
        return _formValidation.data.consent ? "valid" : "invalid";
    }
 
    function _checkSubmitButton() {
        var result = true;
        var resultShouldbe = {
            age: true,
            agreeValidation: true,
            artistName: true,
            consent: _isConsentRequired,
            contantNumber: true,
            email: true,
            name: true,
            termsValidation: true,
            webVideoLink: true,
        };
        for (var item in resultShouldbe) {
            if(_formValidation.data[item] != resultShouldbe[item]){
                result = false;
            }
        }
 
        var isDisabled = !result;
 
        $("#sign-up button[type='submit']").prop("disabled", isDisabled);
        $("#sign-up button[type='submit']").closest(".expanding-btn")[isDisabled ? "addClass" : "removeClass"]("disabled");
    }
 
    function _checkForValidationHandler(e) {
        var $this = $(this);
        var fn = $this.attr("name") + "Validation";
        $this.closest(".form-group").removeClass("valid invalid").addClass(_formValidation[fn]($($this)));
        _checkSubmitButton();
    }
 
    function _addEvents() {
        $(window).on('scroll', function () {
            var cur_pos = $(this).scrollTop();
 
            _sections.each(function() {
                var top = $(this).offset().top - _nav_height,
                    bottom = top + $(this).outerHeight();
 
                if (cur_pos >= top && cur_pos <= bottom) {
                    _nav.find('a').removeClass('active');
                    _sections.removeClass('active');
 
                    $(this).addClass('active');
                    _nav.find('a[href="#'+$(this).attr('id')+'"]').addClass('active');
                }
            });
 
            if (window.pageYOffset >= _sticky) {
                _navbar? _navbar.classList.add("sticky"): null;
            } else {
                _navbar? _navbar.classList.remove("sticky"): null;
            }
        });
 
        _nav.find('a').on('click', _scrollToSection);
        $("#section-1 .sign-up-button").find('a').on('click', _scrollToSection);

        $("form#sign-up").on("submit", _signUpHandler);
        $("form#sign-up").on("reset", _signUpResetHandler);
        $("form#sign-up").on("input change", ":input", _checkForValidationHandler);
    }
    function _scrollToSection() {
        var $el = $(this)
            , id = $el.attr('href');

        $('html, body').animate({
            scrollTop: $(id).offset().top - _nav_height
        }, 500);

        return false;
    }
 
    function _variables() {
        _sections = $('section');
        _nav = $('.sticky-nav');
        _nav_height = _nav.outerHeight();
 
        _navbar = document.getElementById("navigation-section");
        _navbar?_sticky = _navbar.offsetTop:null;
 
        _formValidation = {
            data: {
                artistName: true,
                consent: false
            },
            nameValidation: _nameValidation,
            emailValidation: _emailValidation,
            contactNumberValidation: _contactNumberValidation,
            ageValidation: _ageValidation,
            artistNameValidation: _artistNameValidation,
            webVideoLinkValidation: _webVideoLinkValidation,
            termsValidation: _termsValidation,
            agreeValidation: _agreeValidation,
            consentValidation: _consentValidation
        }
    }
 
    function _getRandomInt(max) {
        return Math.floor(Math.random() * Math.floor(max));
    }
 
    function _randomizeMentorImages(){
        var arrImgs = ["/clientlibs/images/open-call/img_opencall_point01_charlie@2x.png", "/clientlibs/images/open-call/img_opencall_point01_joanna@2x.png", "/clientlibs/images/open-call/img_opencall_point01_kelly@2x.png", "/clientlibs/images/open-call/img_opencall_point01_sezairi@2x.png", "/clientlibs/images/open-call/img_opencall_point01_shabir@2x.png"];
        var randomIndex = _getRandomInt(arrImgs.length);
 
        var $imgElement = $("#section-2 .stage-background").find("img");
        $imgElement.attr("src", arrImgs[randomIndex]);
 
    }

    function _scrollSave(x){
        $('html, body').animate({
            scrollTop: $(x).offset().top 
        }, 60);
    }
 
    function _init() {
        _variables();
        _addEvents();
        _randomizeMentorImages()
    }

    $('.tnc').click(function(e){
        e.preventDefault();
        window.open("clientlibs/documents/tgsr_opencall_tnc.pdf","target=_blank");
        _scrollSave('.tnc')
        return false;
    });
    $('.consent-form').click(function(e){
        e.preventDefault();
        window.open("clientlibs/documents/tgsr_consentform.pdf", "target=_blank");
        _scrollSave('.consent-form')
        return false;
    })
 
    $(_init())
})(jQuery);
 
 
 
  //FUNCTION TO GET AND AUTO PLAY YOUTUBE VIDEO FROM DATATAG
function autoPlayYouTubeModal() {
var trigger = $("body").find('[data-toggle="modal"]');
      trigger.click(function () {
          var theModal = $(this).data("target"),
              videoSRC = $(this).attr("data-theVideo"),
              videoSRCauto = videoSRC;
           
          $(theModal + ' iframe').attr('src', videoSRCauto);
          $(theModal + ' button.close').click(function () {
              $(theModal + ' iframe').attr('src', videoSRC);
          });
          $('.close-video-btn').click(function(){
            $(theModal + ' iframe').attr('src', 'blank');
            $('.nav-bar').css({'opacity':'1','z-index':'99'});
            $('.absolute-header').css('z-index',999);
         });
          setTimeout(function(){
            $('.modal-backdrop').hide();
            $('.nav-bar').css({'opacity':'.1'});
            $('.absolute-header').css('z-index',1);
        },100);
      });
  }
 
  autoPlayYouTubeModal();
