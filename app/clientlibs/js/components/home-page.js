(function ($) {

    let _homeCarousel;


    function _slickInitHandler(e) {
        _homeCarousel.fadeIn();
    }

    function _slickBeforeChangeHandler(e, slick, currentSlide, nextSlide) {
        if(currentSlide < nextSlide){
            $('.tgsr-logo').removeClass('home-carousel-active');
        } else if(currentSlide == nextSlide && nextSlide == 1) {
            $('.tgsr-logo').removeClass('home-carousel-active');
        } else{
            $('.tgsr-logo').addClass('home-carousel-active');
        }

    }

    function _addEvents() {

        _homeCarousel.on("init", _slickInitHandler);
        _homeCarousel.on('beforeChange', _slickBeforeChangeHandler);
        // $(document).on("click", "video", function(e){
        //     e.preventDefault();
        //     return false;
        // })
        
        $(document).on("click", ".music-icon", function (e) {
            $(e.currentTarget).toggleClass("paused");
        })
    }

    function _initializeCarousal() {
        _homeCarousel.slick({
            // centerMode: true,
            // centerPadding: '60px',
            dots: true,
            slidesToShow: 1,
            adaptiveHeight: true,
            swipe: false,
           
            responsive: [
                {
                    breakpoint: 768,
                    
                    settings: {
                        arrows: true,
                        centerMode: true,
                        centerPadding: '40px',
                        slidesToShow: 1,
                        swipe: false
                       
                    }
                },
                {
                    breakpoint: 480,
                    
                    settings: {
                        arrows: true,
                        centerMode: true,
                        centerPadding: '40px',
                        slidesToShow: 1,
                        swipe: false    
                        
                    }
                }
            ]
        });
    }

    function _variablesInitialize() {
        _homeCarousel = $('.home-slider');

        if(location.pathname == "/" || location.pathname.toLocaleLowerCase() == "/index.html"){
            $(".tgsr-logo.home-carousel-active").find("img").attr("src", "/clientlibs/images/global/tgsr_logo.gif");
          } else {
            $(".tgsr-logo.home-carousel-active").removeClass("home-carousel-active");
            
        }
        
    }
    $('.home-slider').click(function(){   
        if ($(".tgsr-logo").hasClass("home-carousel-active")) {
            $('.tgsr-logo').css('display','block');
        }
        else{ 
            $('.tgsr-logo').css('display','none');
        } 
    })
    
    

    function _init() {
        _variablesInitialize();
        _addEvents();
        _initializeCarousal();
    }

    $(_init());
})(jQuery);

